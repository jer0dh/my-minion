const { watch, series, parallel} = require('gulp');

const options = require('./lib/getOptions');


// const {config} = require('./config/');

// const {styles} = require('./tasks/styles');
// const {clean} = require('./tasks/clean');
// const {deploy} = require('./tasks/deploy');
// const {php} = require('./tasks/php');
// const {patch} = require('./tasks/version');
// const {filesCopy} = require('./tasks/files');
// const {javascript} = require('./tasks/javascript');





// exports.patch = patch;
// exports.deploy = deploy;
// exports.default = series(clean, filesCopy, parallel( styles, php, javascript), deploy, watchTasks);



  



const { styles, stylesSrcFolder } = require('./tasks/styles');
const {javascript, javascriptSrcFolder } = require('./tasks/javascript');
const {php, phpSrcFolder } = require('./tasks/php');
const { filesCopy } = require('./tasks/files');

function watchTasks() {
      watch( [stylesSrcFolder + '/**/*.scss'], series( styles) );
      watch( [phpSrcFolder + '/**/*.php'], series( php ));
      watch( [javascriptSrcFolder + '/**/*.js'], series( javascript ));

  }

let tasks = [ styles, javascript, php, filesCopy ];

  exports.default = series( tasks, watchTasks );
// define styles, deploy, clean, php const, watchTasks function

// args could be used to turn off some tasks and some watches

