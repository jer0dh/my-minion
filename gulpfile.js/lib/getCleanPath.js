// Remove any backspace at the end and possible future things

const getCleanPath = (path="") => {
    return path.replace(/\/$/, '').replace(/^\.\//,'');
     
};

exports.getCleanPath = getCleanPath;
