const { arg } = require('./getCommandLine');
const fs = require('fs');
const { getCleanPath } = require('./getCleanPath');
const { getPath } = require('./getPath');

let options = {}

if (typeof (arg['p']) === 'undefined') {

    console.log(' need --p path to know where I am working');
    return;
}
arg['p'] = getCleanPath(arg['p']);
const path = arg['p'];
try {
  if (fs.existsSync(path)) {
      if (fs.existsSync(path + '/my-minion-instructions.json')) {
        options = require(path + '/my-minion-instructions.json');
    } else {
        console.log('Cannot find ' + path + '/my-minion-instructions.json')
        return;
    }
  } else {
    console.log('Cannot find path: ' + path);
    return; // if we cannot find path exit.
  }
} catch(err) {
  console.log(err)
}


options['path'] = path;

const styleSrcFolder = getPath(options.stylePathAbsolute, options.styleSrc, options.path);
const javascriptSrcFolder = getPath(options.javascriptPathAbsolute, options.javascriptScriptSrc, options.path);
const phpSrcFolder = getPath(options.phpPathAbsolute, options.phpSrc, options.path);

options['doNotCopyList'] = [
        '!' + styleSrcFolder + '/**/*.*',
        '!' + javascriptSrcFolder + '/**/*.*',
        '!' + phpSrcFolder + '/**/*.php',
//        '!' + config.srcFolder + 'assets/style.css',
    ];

console.log(options)

exports.options = options