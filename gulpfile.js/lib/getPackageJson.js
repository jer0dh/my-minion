const fs = require('fs');
const { options } = require('./getOptions');
const { getCleanPath } = require('./getCleanPath');

const getPackageJson = () => {
    if (typeof (options.packageJSONPath) !== 'undefined' && options.packageJSONPath !== '') {
        return JSON.parse(fs.readFileSync( getCleanPath(options.packageJSONPath) + '/package.json', 'utf8'));
    } else if ( fs.existsSync(options.path + '/package.json') ) {
        return JSON.parse(fs.readFileSync(options.path + '/package.json', 'utf8'));
    } else {
        return {}
    }
};

exports.getPackageJson = getPackageJson;
