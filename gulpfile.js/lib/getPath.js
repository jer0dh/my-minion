// Excepts 
const { getCleanPath } = require('./getCleanPath');

const getPath = (isAbsolute = false, inputedPath, projectPath ) => {
    if (!isAbsolute) {
        return getCleanPath( getCleanPath(projectPath) + '/' + getCleanPath( inputedPath ) );  //if inputedPath is empty, need to remove added '/'
    } else {
        return getCleanPath(inputedPath);
    }
};

exports.getPath = getPath;