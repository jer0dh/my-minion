# My Minion

A standalone task runner built with gulp.  The purpose is to have an independent system running tasks on any project I'm working on and to allow any updates and changes apply to all projects.  Also helps when an old project is revived or needs edits and discovering it needs its task runner system updated before anything else.

Run `gulp --p [path]` where path is the path to your project containing a my-minion-instructions.json file containing specific configuration for this project