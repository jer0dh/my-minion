const { src, dest } = require('gulp');
const { options } = require('../lib/getOptions');
const { getPath } = require('../lib/getPath');
const newer = require('gulp-newer');

function filesCopy() {
    const srcFolder = getPath(options.filesPathAbsolute, options.filesSrc, options.path);
    const destFolder = getPath(options.filesPathAbsolute, options.filesDest, options.projectDestFolder);
    return src([srcFolder + '/**/*.*'].concat(options.doNotCopyList))
        .pipe(newer( destFolder ))
        .pipe(dest( destFolder ));
}

exports.filesCopy = filesCopy