const { src, dest, parallel } = require('gulp');
const { options } = require( '../lib/getOptions' );
const filter = require('gulp-filter');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const minify = require('gulp-minify');
const rename = require('gulp-rename');

// https://stackoverflow.com/questions/38263316/is-file-path-valid-in-gulp-src
const glob = require('glob');
const { getPath } = require('../lib/getPath');


//Checks to see if file exists.  If not, show error and none of the files are processed.
function checkSrc(paths) {
    paths = (paths instanceof Array) ? paths : [paths];
    var existingPaths = paths.filter(function(path) {
      if (glob.sync(path).length === 0) {
          console.log('\x1b[41m *** ERROR: ' + path + ' doesn\'t exist ***\x1b[0m');
          return false;
      }
      return true;
    });
    return src((paths.length === existingPaths.length) ? paths : []);
  }

// add complete path to script arrays


const javascriptScripts = options.javascriptScripts.map( (s) => getPath( options.javascriptPathAbsolute, s, options.path));

const javascriptVendorScripts = options.javascriptVendorScripts.map( (s) => getPath( options.javascriptPathAbsolute, s, options.path));

const javascriptScriptSrc = getPath( options.javascriptPathAbsolute, options.javascriptScriptSrc, options.path )
const javascriptDest = getPath( options.javascriptPathAbsolute, options.javascriptDest, options.projectDestFolder );

console.log( javascriptScriptSrc )
// Concatenate arrays of scripts
const allScripts = javascriptVendorScripts.concat( javascriptScripts );
const negatedProjectVendorScripts = javascriptVendorScripts.map( (s) => '!' + s);
//const negatedProjectScripts = config.projectScripts.map( (s) => '!' + s);
const negatedAllScripts = allScripts.map( (s) => '!' + s);


// Creates the main script using the projectVendorScripts and projectScripts in package.json
//      Runs babel on projectScripts
//      Concats scripts based on order in array into a file defined by projectScript
//      Creates a minified version
function mainScript( cb ) {
    // A filter to only use scripts in projectScripts - take out vendor scripts
    const noVendorFilter = filter( negatedProjectVendorScripts.concat( options.javascriptScripts ), {restore: true});
    if (allScripts && allScripts.length) {
    return checkSrc( allScripts )
    .pipe(sourcemaps.init())
    .pipe( noVendorFilter ) //no babel-ing of vendor files
    .pipe(babel())
    .pipe( noVendorFilter.restore ) //put the vendor files back
    .pipe( concat( options.javascriptScriptName )) //put all files into single file with projectScriptName
   // .pipe( dest( config.destFolder + '/js')) // copy unminized js to destination - gulp minify appears to keep unminified file in stream
    .pipe( minify({
        ext: {
            min: '-min.js'
        }
    }))
    .pipe(sourcemaps.write('./maps'))
    .pipe( dest( javascriptDest ));
}
    else {
    	cb();
	}
}

// Babels Minifies any other javascript files under /js and moves to destination
//      except for files in the projectScript arrays, *.min.js files, or files in the js/vendor directory
function minorScripts() {
    return src([javascriptScriptSrc + '/**/*.js', '!'+ javascriptScriptSrc +'/vendor/**/*.js' ].concat(negatedAllScripts))
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(minify({
        ext: {
            min: '-min.js'
        }
    }))
    .pipe(sourcemaps.write('./maps'))
    .pipe( dest( javascriptDest ))
}



exports.javascript = parallel(mainScript, minorScripts);
exports.javascriptSrcFolder = javascriptScriptSrc;
