const { series, src, dest } = require('gulp');
const phplint = require('gulp-phplint');
const template = require('gulp-template');

const {getPackageJson} = require('../lib/getPackageJson');
const newer = require('gulp-newer');
const cached = require('gulp-cached');
const through2 = require('through2');
const {modTime} = require('../lib/modTime');

const { options } = require('../lib/getOptions');
const { getPath } = require('../lib/getPath');
const srcFolder = getPath(options.phpPathAbsolute, options.phpSrc, options.path);
const destFolder = getPath(options.phpPathAbsolute, options.phpDest, options.projectDestFolder);
    
function phpLint() {
    console.log( 'npm i -g phplint to install phplint globally');

    return src([srcFolder + '/**/*.php'])
        .pipe(cached('phpLint'))
        .pipe( phplint() )
        .pipe(phplint.reporter(function(file){
            let report = file.phplintReport || {};
            if (report.error) {
                console.error(report.message+' on line '+report.line+' of '+report.filename);
            }
        }));
}

function phpTemplateCopy() {

    return src([srcFolder + '/**/*.php'])
        .pipe(template({pkg: getPackageJson(), production: options.production }))
        .pipe(newer( destFolder ))
        .pipe(through2.obj( modTime ))
        .pipe(dest( destFolder ));
}

const availableTasks = {    phpLint:            phpLint, 
                            phpTemplateCopy:    phpTemplateCopy };

let runTasks = [ phpLint, phpTemplateCopy ];

if (typeof ( options.phpTasks ) !== 'undefined' ) {
    runTasks = [];
 for (const task in availableTasks) {
     if ( typeof( options.phpTasks[task] ) !== 'undefined' && options.phpTasks[task] ){
         runTasks.push( availableTasks[task] );
     }
 }
}
exports.php = series(runTasks);
exports.phpSrcFolder = srcFolder;
