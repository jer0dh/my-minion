const { src, dest, parallel } = require('gulp');

const { options } = require('../lib/getOptions');
const { getPath } = require('../lib/getPath');

const { getPackageJson }= require( '../lib/getPackageJson'); 
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const template = require('gulp-template');
const srcFolder = getPath(options.stylePathAbsolute, options.styleSrc, options.path);
const destFolder = getPath(options.stylePathAbsolute, options.styleDest, options.projectDestFolder);
//const {getPackageJson} = require('../lib/getPackageJson');

// Create CSS files unminified with source maps
function stylesMaxSass() {
    
    return src(srcFolder + '/**/*.scss')
        .pipe(sourcemaps.init())
        //       .pipe(template( {pkg:getPackageJson() }))
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({}))
        .pipe(sourcemaps.write('./maps'))
        .pipe(dest(destFolder));
}

// Create CSS files minified with source maps
function stylesMinSass() {
    
    return src( srcFolder + '/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed', sourceMap: true}).on('error', sass.logError))
        .pipe( autoprefixer())
        .pipe(rename(function(path){
            path.extname = '-min.css'
        }))
        .pipe(sourcemaps.write( './maps'))
        .pipe(dest( destFolder ))
}

// Creation of style.css from packageJson
 function stylesTemplateCss() {
    return src( options.path + '/style.scss')
        .pipe(template( {pkg:getPackageJson() }))
        .pipe(sass().on('error', sass.logError))
        .pipe( autoprefixer())
        .pipe(dest( options.path ))
}

exports.styles = parallel(stylesMaxSass, stylesMinSass, stylesTemplateCss );
exports.stylesSrcFolder = srcFolder;

